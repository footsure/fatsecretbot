<?php 

namespace FatSecret;

use Symfony\Component\DomCrawler\Crawler;

use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class Parser {

	static $client;

	static $concurrency = 5;

	static $retries = 3; // количество попыток выполнить HTTP-запрос, завершившийся ошибкой
	static $delay = 500; // длительность паузы между двумя последовательными HTTP-запросами
	static $pause = 0;   // флаг состояния останова процесса парсинга
	static $count = 0;   // счетчик HTTP-запросов
	static $timeout = [ 'pause' => 30 /*seconds*/, 'after' => 50 /*requests*/];
	static $cache   = [];
	static $failed  = [];

	static $items = []; 
	static $links = [];

	static function setup(array $options = []) {

		if (self::$items) self::$items   = [];
		if (self::$links) self::$links   = [];
		if (self::$failed) self::$failed = [];

		if ($options) foreach (['concurrency', 'retries', 'delay', 'cache', 'timeout'] as $option) {

		 	if (isset($options[$option])) {

		 		if (is_int($options[$option]) && is_int(self::${$option})) {

			 		self::${$option} = $options[$option];

		 		} elseif (is_array($options[$option]) && is_array(self::${$option})) {

		 			self::${$option} = array_merge(self::${$option}, $options[$option]);
		 		};
		 	};
		};

		if (!isset(self::$client) || ($options && array_intersect_key(array_flip(['delay', 'cache']), $options))) {

			$params = ['base_uri' => 'http://www.fatsecret.ru/калории-питание/', 'cookies' => true];

			if (self::$delay) $params['delay'] = self::$delay; 

			if (self::$cache && self::$cache['ttl'] > 0) {

				if (class_exists('League\Flysystem\Adapter\Local') &&
					class_exists('Kevinrob\GuzzleCache\CacheMiddleware') &&
					class_exists('Kevinrob\GuzzleCache\Storage\FlysystemStorage') && 
					class_exists('Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy')) {

					$stack = HandlerStack::create();
					$stack->push(
					  new \Kevinrob\GuzzleCache\CacheMiddleware(
					    new \Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy(
					      new \Kevinrob\GuzzleCache\Storage\FlysystemStorage(
					        new \League\Flysystem\Adapter\Local(self::$cache['path'] ?? dirname(__FILE__).'/../cache')
					      ),
					      self::$cache['ttl'] // TTL in seconds
					    )
					  ), 
					  'cache'
					);
					$params['handler'] = $stack;

				} else {
					trigger_error('Невозможно активировать кеширование HTTP-запросов. 
						Убедитесь, что пакеты kevinrob\guzzlecache и league\flysystem установлены и корректно подключены.');
				};
			};

			self::$client = new Client($params);
		};
	}

	static function getFood (string $url, array $filters = []) {

		self::setup();

		try {

			self::getItems(self::getItemsLinks($url, $filters));
			
		} catch (\Exception $e) {
		// } catch (RequestException $e) {

			self::$items  = [];
			self::$links  = [];
			self::$failed = [['error_code' => $e->getCode(), 'error_message' => urldecode($e->getMessage())]];
		}
		return self::$items;
	}

	static function getFoodInfo (string $url) {

		self::setup();

		try {

			self::getItems(new \ArrayIterator([$url]));
			
		} catch (\Exception $e) {
		// } catch (RequestException $e) {

			self::$items  = [];
			self::$links  = [];
			self::$failed = [['error_code' => $e->getCode(), 'error_message' => urldecode($e->getMessage())]];
		}
		return empty(self::$items) ? false : self::$items[0];
	}

	static function getBrands (int $type, array $alphabet = []) {

		if (empty($alphabet)) {

		   $alphabet = ['A','B','C','D','E','F','G',
						'H','I','J','K','L','M','N',
						'O','P','Q','R','S','T','U',
						'V','W','X','Y','Z','А','Б',
						'В','Г','Д','Е',    'Ж','З',
						'И','Й','К','Л','М','Н','О',
						'П','Р','С','Т','У','Ф','Х',
						'Ц','Ч','Ш','Щ','Ъ','Ы','Ь',
						'Э','Ю','Я','*'           ];
		};

		self::setup();

		try {

			foreach ($alphabet as $key) {

				$url = sprintf('/Default.aspx?pa=brands&f=%s&t=%d', $key, $type);

				$selector = sprintf('h2 > a[href^="/%s/"]', urlencode('калории-питание'));
				
				$items = self::getItemsLinks($url, [], $selector, ['href', 'title']);

				foreach ($items as $item) {

					self::$items[] = [
						'name' => $item[1],
						'url'  => 'www.fatsecret.ru'.urldecode($item[0]),
						'type' => $type
					];
				}
			}

		} catch (\Exception $e) {
		// } catch (RequestException $e) {

			self::$items  = [];
			self::$links  = [];
			self::$failed = [['error_code' => $e->getCode(), 'error_message' => urldecode($e->getMessage())]];
		}
		return self::$items;
	}

	static function getManufacturers(array $alphabet = []) {
		return self::getBrands(1, $alphabet);
	}

	static function getRestaurants(array $alphabet = []) {
		return self::getBrands(2, $alphabet);
	}

	static function getSupermarkets(array $alphabet = []) {
		return self::getBrands(3, $alphabet);
	}

	static private function getItems(\Iterator $links) {

		$client = self::$client;

		$failed = [];

		$fails = 0;

		$requests = function ($links) {

		    foreach ($links as $key => $uri) {

		    	if ( self::$pause || self::$timeout['after'] > 0 && ++self::$count > self::$timeout['after'] ) {
		    		sleep(self::$timeout['pause']);
		    		self::$pause = false;
		    		self::$count = 0;
		    	}

		    	self::$links[$key] = urldecode($uri);
	    		
		        yield $key => new Request('GET', $uri);
		    }
		};

		do {

			$pool = new Pool(self::$client, $requests($links), [

			    'concurrency' => self::$concurrency,

			    'fulfilled' => function ($response, $index) {

			        $crawler  = new Crawler();
			        $crawler->addHtmlContent($response->getBody()->getContents());

			        $matched_label = $crawler->filter('.nutpanel .label')->extract('_text');

			        if ($matched_label) {
				        preg_match("/Размер Порции:(.*)/", $matched_label[0], $matches);
			        };

			        if (isset($matches) && !empty($matches[1])) {

			        	$unite = trim($matches[1]);

			        } else $unite = null;

			        if ($unite === '100г (100 г)') {

			        	$unite = '100г';

			        } elseif ($unite === '100мл (100 мл)') {

			        	$unite = '100мл';
			        };

			        $facts = $crawler->filter('.factPanel .fact .factValue')->extract('_text');

			        preg_match("/rid=(\d*)/i", $crawler->filter('#updateForm')->extract('action')[0], $matches);
			        $rid = $matches[1] ?? null;

			        $input = $crawler->filter('[name=portionid]');

			        if (count($input)) {

				        if ($input->nodeName() === 'input') {
				        	$portions = [];
				        } elseif ($input->nodeName() === 'select') {
				        	$options = $input->filter('option')->extract(['value', '_text']);
				        	foreach ($options as $option) {
				        		$portions[] = ['id' => $option[0], 'desc' => $option[1]];
				        	}
				        }
			        }

			        self::$items[] = [
			        	'name'     => $crawler->filter('.breadCrumb .breadcrumb_noLink')->extract('_text')[0],
			        	'brand'    => $crawler->filter('.manufacturer > a')->extract('_text')[0] ?? '',
			        	'url'      => $index,
			        	'unite'    => $unite,
			        	'calorie'  => $facts[0],
			        	'fat'      => $facts[1],
			        	'carb'     => $facts[2],
			        	'protein'  => $facts[3],
			        	'rid'      => $rid,
			        	'portions' => $portions,
			        ];
			    },
			    'rejected' => function ($reason, $index) use (&$failed) {

			    	$link = self::$links[$index];

			    	if ($link) {

			    		unset(self::$links[$index]);

			    		self::$pause = true;

			    		self::$failed[$link] = isset(self::$failed[$link]) ? self::$failed[$link] + 1 : 1;

			    		if (self::$failed[$link] === self::$retries)

			    			self::$failed[$link] = ['error_code' => $reason->getCode(), 'error_message' => urldecode($reason->getMessage())];

			    		else $failed[$index] = $link;
			    	}
			    }
			]);

			$pool->promise()->wait();

			$links = $failed;

			$failed = [];

		} while (count($links) > 0);

		if (self::$items) foreach (self::$items as $key => $item) {
			self::$items[$key]['url'] = self::$links[$item['url']] ?? null;
		};
	}

	static private function getItemsLinks (
		string $url,
		array  $filters = [],
		string $link_selector = 'a.prominent',
		array  $link_props = ['href'] 
	) {

		$retries  = 0;
		$next     = $url;
		$crawler  = new Crawler();

		do {

			try {

				$response = self::$client->get($next);

			} catch (RequestException $e) {

				if (++$retries < self::$retries) {
					self::$pause = true;
					continue;
				} else throw $e;

			} $retries = 0;

			$html = $response->getBody()->getContents();

			$crawler->clear();

			$crawler->addContent($html);

			$elems = $crawler->filter($link_selector);
			$links = $elems->extract($link_props);

			if ($links) foreach ($links as $key => $link) {
				if ($link) {
					if ($filters) {

						if (!empty($filters['urlHas'])) {

							if (strpos(urldecode($link), $filters['urlHas']) !== false) {

								yield $link;
							}
						};

						/* $brand_uri = $elems[$key]->nextAll()->filter('a.brand')->extract('href');

						if (count($brand_uri) == 1) {

							if (strpos(urldecode($brand_uri), $filter) !== false) {

								yield $link;
							}
						} */

					} else yield $link;
				}
			};
			
			$matched_href = $crawler->filter('.searchResultsPaging .strong + a')->extract('href');

			$next = $matched_href ? $matched_href[0] : false;

		} while ($next);
	}
}