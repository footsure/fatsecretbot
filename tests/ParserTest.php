<?php

namespace FatSecret\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Promise\FulfilledPromise;
use Symfony\Component\DomCrawler\Crawler;

class Parser extends \FatSecret\Parser {

	static $container = [];

	static function setup(array $options = []) {

		parent::setup($options);

		self::$container = [];

		$config = self::$client->getConfig();

		if (isset($config['handler'])) {
			$stack = $config['handler'];
			if (get_class($stack) !== 'GuzzleHttp\HandlerStack') {
				trigger_error("\$config['handler'] must be an instance of GuzzleHttp\HandlerStack class");
			}
		} else {
			$stack = HandlerStack::create();
		}

		$history = Middleware::history(self::$container);

		self::pushMiddleware($stack, $history, 'history', 'cache');
		
		if (isset($options['throttle'])) {
			self::pushMiddleware($stack, $options['throttle'], 'throttle', 'cache');
		}
		$config['handler'] = $stack;
		self::$client = new Client($config);
	}

	static function pushMiddleware(HandlerStack &$stack, callable $middleware, string $alias, string $before = '', string $after = '') {

		if ($before) {

			$mode = 'before';
			$name = $before;

		} elseif ($after) {

			$mode = 'after';
			$name = $before;

		} else return $stack->push($middleware, $alias);

		try {
			return $stack->{$mode}($name, $middleware, $alias);
		} catch (\InvalidArgumentException $e) {
			return $stack->push($middleware, $alias);
		}
	}

	/** @return int[$pages, $foods] */
	static function getMetrics($url) {

		$response = self::$client->get($url);
		$html = $response->getBody()->getContents();

		$crawler = new Crawler();
		$crawler->addHtmlContent($html);

		preg_match("/(\d*) из (\d*)$/", trim($crawler->filter('.searchResultSummary')->extract('_text')[0]), $matches);

		if (count($matches) == 3) {

			$foods = intval($matches[2]);
			$amount = intval($matches[1]);

			if ($foods > $amount) {
				$pages = intdiv($foods, $amount) + ($foods % $amount > 0 ? 1 : 0);
			} else $pages = 1;

			return [$pages, $foods];
		}
	}
}

class ParserTest extends \PHPUnit_Framework_TestCase {

	public function testSettings() {

		$path = dirname(__FILE__).'/cache';

		Parser::setup([
			'concurrency' => 8, 
			'retries' => 2,
			'cache' => ['ttl' => 3600, 'path' => $path],
			'delay' => 300 /*ms, пауза между двумя последовательными запросами*/,
			'timeout' => ['pause' => 20 /*seconds*/, 'after' => 100 /*requests*/]
		]);

		$this->assertInstanceOf('GuzzleHttp\Client', Parser::$client);
		$this->assertEquals(8, Parser::$concurrency);
		$this->assertEquals(2, Parser::$retries);
		$this->assertEquals(300, Parser::$delay);
		$this->assertEquals(3600, Parser::$cache['ttl']);
		$this->assertEquals($path, Parser::$cache['path']);
		$this->assertEquals(20, Parser::$timeout['pause']);
		$this->assertEquals(100, Parser::$timeout['after']);

		$config = Parser::$client->getConfig();
		$this->assertEquals(300, $config['delay']);

		$this->assertInstanceOf('GuzzleHttp\HandlerStack', $config['handler']);

		preg_match_all("/Name: '(\w*)', Function/", (string) $config['handler'], $matches);

		if ($matches[1]) $middleware = $matches[1];
		else $this->fail('Middleware stack is empty');

		foreach (['cache', 'history'] as $name) {
			$this->assertContains($name, $middleware, "<$name> middleware must be in stack");
		}
	}

	public function testPagination() {

		$url = 'search?q='.'ашан+йогурт';

		Parser::setup([
			'concurrency' => 5,
			'retries'     => 3,
			'cache'       => ['ttl'   => 36000],
			'timeout'     => ['pause' => 1],
		]);

		list($pages_number) = Parser::getMetrics($url);

		$items = Parser::getFood($url);

		foreach (Parser::$container as $transaction) {

			if ($transaction['response'] && $transaction['response']->getStatusCode() === 200) {

			    $path = $transaction['request']->getUri()->getPath();

			    if (substr($path, -6) === 'search') {

				    $query = $transaction['request']->getUri()->getQuery();
				    parse_str($query, $params);
				    $pages[$query] = $params['pg'] ?? 0;
			    }
			}
		};
		$this->assertCount($pages_number, array_keys($pages),
			'Not all pagination\'s pages have been processed (matching by count)');
		$this->assertEquals(range(0, $pages_number-1), array_values($pages),
			'Not all pagination\'s pages have been processed (matching by query params)');
	}

	public function testFoodsPagesAmount() {

		$url = 'search?q='.'ашан+йогурт';

		Parser::setup([
			'concurrency' => 5,
			'retries'     => 3,
			'cache'       => ['ttl'   => 36000],
			'timeout'     => ['pause' => 1],
		]);

		list( , $foods_number) = Parser::getMetrics($url);

		$items = Parser::getFood($url);

		$this->assertCount($foods_number, $items, 'Not all food\'s pages have been processed');
	}

	public function testFoodPageSelectors() {

		$test_cases = [
			'Йогурт Ванильный' =>
			[
				'url' => '/калории-питание/Ашан/Йогурт-Ванильный/100г',
				'matched' => false
			],
			'Fromage Blanc 0%' =>
			[
				'url' => '/калории-питание/Ашан/fromage-blanc-0/100г',
				'matched' => false
			],
			'Печенье с молоком и кусочками шоколада' =>
			[
				'url' => '/калории-питание/Ашан/Печенье-с-Молоком-и-Кусочками-Шоколада/4-печенья',
				'matched' => false
			],
		];

		$url = 'search?q='.'ашан+йогурт';

		Parser::setup([
			'concurrency' => 3,
			'retries'     => 3,
			'cache'       => ['ttl'   => 36000],
			'timeout'     => ['pause' => 1],
		]);

		$items = Parser::getFood($url);

		foreach ($items as $item) {

			if ($item['url'] == $test_cases['Йогурт Ванильный']['url']) { //калории-питание/Ашан/Йогурт-Ванильный/100г

				$this->assertEquals('Йогурт Ванильный', $item['name']);

				$this->assertEquals('Ашан', $item['brand']);

				$this->assertEquals('100г', $item['unite']);

				$get_float = function($val) {
					$val = str_replace(',', '.', $val);
					return (float) $val;
				};

				$this->assertEquals(111,  $get_float($item['calorie']));
				$this->assertEquals(4.5,  $get_float($item['fat']));
				$this->assertEquals(13.2, $get_float($item['carb']));
				$this->assertEquals(4.3,  $get_float($item['protein']));

				$this->assertEquals('4806409', $item['rid']);

				$this->assertEmpty($item['portions']);

				$test_cases['Йогурт Ванильный']['matched'] = true;
			
			} elseif ($item['url'] == $test_cases['Fromage Blanc 0%']['url']) { //калории-питание/Ашан/fromage-blanc-0/100г

				$this->assertEquals(0, (float)$item['fat']);

				$test_cases['Fromage Blanc 0%']['matched'] = true;
			}
		}

		$url = 'search?q='.'Ашан+Печенье';

		$items = Parser::getFood($url);

		foreach ($items as $item) {

			//калории-питание/Ашан/Печенье-с-Молоком-и-Кусочками-Шоколада/4-печенья
			if ($item['url'] == $test_cases['Печенье с молоком и кусочками шоколада']['url']) { 

				$this->assertEquals('4 печенья (50 г)', $item['unite']);

				$portions = [
					['id' => '7202742', 'desc' => '4 печенья (50г)'],
					['id' => '-1', 'desc' => 'г'],
				];

				$this->assertEquals($portions, $item['portions']);

				$test_cases['Печенье с молоком и кусочками шоколада']['matched'] = true;
			}
		}

		foreach ($test_cases as $product => $test_case) {
			if (!$test_case['matched']) {
				$this->fail(sprintf('Not found `%s` (%s)', $product, $test_case['url']));
			}
		}
	}

	public function testThrottlingRequests() {

		// Тестирование функциональности парсера при 100% уровне отказов

		$throttle = new ThrottlingMiddleware(0);

		$retries = 3;

		$url = 'search?q='.'ашан+йогурт';

		Parser::setup([
			'concurrency' => 8, 
			'retries' => $retries,
			'cache' => ['ttl' => 0],
			'timeout' => ['pause' => 1],
			'throttle' => $throttle,
		]);

		$items = Parser::getFood($url);

		$this->assertEquals(Parser::$retries, $throttle->count, 'Not all attempts to process an unsuccessful request have been used');

		// Testing the correctness of processing parsing errors when the number of available attempts is exhausted
		$this->assertEmpty($items);
		$this->assertEmpty(Parser::$links);
		$this->assertEquals(1, count(Parser::$failed));

		$this->assertArrayHasKey('error_code', Parser::$failed[0]);
		$this->assertEquals(429, Parser::$failed[0]['error_code']);

		// Тестирование функциональности парсера при 50% уровне отказов

		$throttle = new ThrottlingMiddleware(0.5);

		$retries = 10;

		Parser::setup([
			'concurrency' => 8, 
			'retries' => $retries,
			'cache' => ['ttl' => 36000],
			'timeout' => ['pause' => 1],
			'throttle' => $throttle,
		]);

		list($pages, $foods) = Parser::getMetrics($url);

		$items = Parser::getFood($url);

		// исключаем из набора страницы пагинации
		if (isset($throttle->failed['/калории-питание/search'])) 
			unset($throttle->failed['/калории-питание/search']);

		$this->assertEquals($throttle->failed, Parser::$failed, 'Not all failed requests have been processed');
		$this->assertCount($foods, $items, 'Not all foods have been parsed');

		// r($items, Parser::$links, Parser::$failed, $throttle->count, $throttle->failed);
	}

	public function testGettingSingleProductInfo() {

		$items = Parser::getFood('search?q='.'ашан+йогурт');

		$item = Parser::getFoodInfo('/%D0%BA%D0%B0%D0%BB%D0%BE%D1%80%D0%B8%D0%B8-%D0%BF%D0%B8%D1%82%D0%B0%D0%BD%D0%B8%D0%B5/%D0%90%D1%88%D0%B0%D0%BD/%D0%99%D0%BE%D0%B3%D1%83%D1%80%D1%82-%D0%92%D0%B0%D0%BD%D0%B8%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9/100%D0%B3');

		$this->assertContains($item, $items);
	}

	public function testFilteringItemsList() {

		$items = Parser::getFood('search?q=3+Корочки', ['urlHas'=>"3-Корочки"]);

		$item = Parser::getFoodInfo('/калории-питание/3-Корочки/Сухарики/100г');

		$this->assertCount(1, $items);
		$this->assertContains($item, $items);
	}

	public function testGettingBrands() {

		$key = 'A'; $type = 1;

		$url = sprintf('/Default.aspx?pa=brands&f=%s&t=%d', $key, $type);

		Parser::setup(['delay' => 1000, 'cache' => ['ttl' => 36000]]);

		list($pages_number, $brands_number) = Parser::getMetrics($url);

		$items = Parser::getManufacturers([$key]);

		foreach (Parser::$container as $transaction) {

			if ($transaction['response'] && $transaction['response']->getStatusCode() === 200) {

			    $path = $transaction['request']->getUri()->getPath();

			    if (substr($path, -12) === 'Default.aspx') {

				    $query = $transaction['request']->getUri()->getQuery();
				    parse_str($query, $params);
				    $pages[$query] = $params['pg'] ?? 0;
			    }
			}
		};

		$item = [ 
			'name' => "Alisa",
            'url'  => "www.fatsecret.ru/калории-питание/alisa",
            'type' => 1
        ];

		$this->assertCount($pages_number, array_keys($pages),
			'Not all pagination\'s pages have been processed (matching by count)');
		$this->assertEquals(range(0, $pages_number-1), array_values($pages),
			'Not all pagination\'s pages have been processed (matching by query params)');
		$this->assertCount($brands_number, $items, 'Not all brands\'s pages have been processed');
		// $this->assertContains($item, $items);
	}

}

class ThrottlingMiddleware {

	public $failed = [];
	public $count = 0;

	private $every = false;
	private $mode = 'reject';

	public function __construct(float $throughput = 0.7) {

		if     ($throughput >= 1) return;
		elseif ($throughput <= 0) $every = 1;

		elseif ($throughput > 0.5) $mode = 'reject';
		elseif ($throughput < 0.5) $mode = 'resolve';
		elseif ($throughput == 0.5) $every = 2;

		if (isset($every)) {
			$this->every = $every;
		} else {
			$this->every = ($mode == 'reject') ? round(1/(1-$throughput), 0, PHP_ROUND_HALF_UP)
											   : round(1/$throughput, 0, PHP_ROUND_HALF_UP);
		}
	}

	private function needReject() {

		$this->count++;

		if ($this->every === false) return false;

		if ($this->every == 1) return true;

		$match = $this->count % $this->every == 0;

		if ($this->mode == 'reject')

			return $match;

		else return !$match;
	}

	public function __invoke(callable $handler) {

	    return function (RequestInterface $request, array $options) use ($handler) {

	    	if ($this->needReject()) {

	    		$path = urldecode($request->getUri()->getPath());

	    		if (isset($this->failed[$path])) 
	    			$this->failed[$path]++;
	    		else $this->failed[$path] = 1; 

		    	return new FulfilledPromise(new Response(429));

	    	} return $handler($request, $options);
	       
	    };
	}
}