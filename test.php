<?php 

require 'vendor/autoload.php';

if (class_exists('ref')) {

	\ref::config('validHtml', true);
	// ref::config('showIteratorContents', true);
	// ref::config('expLvl', 3); // default 1
	\ref::config('maxDepth', 8); // default 6
	\ref::config('showPrivateMembers', true);
}

// $whoops = new \Whoops\Run;
// $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
// $whoops->register();

set_time_limit(0);

use FatSecret\Parser;

Parser::setup([

	'concurrency' => 10, 
	'cache' => ['ttl' => 3600, 'path' => dirname(__FILE__).'/cache'],
	'delay' => 500 /*ms, пауза между двумя последовательными запросами*/,
	'timeout' => ['pause' => 30 /*seconds*/, 'after' => 100 /*requests*/]
]);

if ($_GET['action'] === 'search' && $_GET['q']) {

	$items = Parser::getFood($_GET['action'].'?q='.$_GET['q'].'&pg=0');

	r($items, Parser::$links, Parser::$failed);
}
